
**# Project Description**

A simple project which allows user to capture a photo using the front camera, analyses their emotion from
categories such as happy,sad,angry or neutral and cracks a joke if the person in angry, compliments their smile if the person 
is happy and gives a motivational quote if otherwise

**## Usage**

The user just needs to enter their Anthropic API Key( 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 )
and proceed to click an Image.

**## Credits**

**claude-3-haiku-20240307** model is used to analyze the person's emotion from the photo and is also used 
for the message in the popup.


