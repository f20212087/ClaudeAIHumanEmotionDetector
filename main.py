from user_interface import UserInterface
from image_describer import ImageDescriber
from image_capturer import ImageCapturer
from text_question import TextQuestion
from emotion_handler import EmotionHandler
from popup_input import PopupInput

if __name__ == '__main__':
    api_key = UserInterface.get_api_key()
    image_describer = ImageDescriber(api_key)
    print("API key successfully set.")
    capturer = ImageCapturer()
    capturer.capture_and_save_image()

    # Describe the captured image
    emotion = image_describer.describe_image_with_tkinter("./captured_image.jpg",
                                                 "in only one word, tell me if person is angry, sad, or happy.")


    text_questioner = TextQuestion(api_key)
    emotion_handler = EmotionHandler()
    question = emotion_handler.get_message(emotion)
    response = text_questioner.ask_question(question)
    popup_input = PopupInput()
    popup_input.display_popup(response)

#api_key = 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