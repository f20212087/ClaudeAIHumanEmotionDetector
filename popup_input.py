import tkinter as tk
from tkinter import messagebox

class PopupInput:
    def __init__(self):
        self.root = tk.Tk()
        self.root.title("Popup Input")

    def destroy_popup(self, popup_window):
        popup_window.destroy()
        self.root.quit()

    def display_popup(self, message, duration=10):
        # Destroy the main Tkinter window
        self.root.destroy()

        # Create a new Tkinter window for the popup
        popup_window = tk.Tk()
        popup_window.withdraw()  # Hide the window
        popup_window.title("Popup Message")

        # Display a popup message
        messagebox.showinfo("Message", message)

        # Schedule the destruction of the popup window after a certain time interval
        popup_window.after(duration * 500, self.destroy_popup, popup_window)

        # Run the popup window's event loop
        popup_window.mainloop()