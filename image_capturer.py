import cv2


class ImageCapturer:
    def __init__(self):
        self.cap = cv2.VideoCapture(0)
        if not self.cap.isOpened():
            print("Error: Unable to open camera.")
            return

    def capture_and_save_image(self):
        print("Press Enter to start image capture process and Press Enter to save image...")
        # Wait for the Enter key press
        input()

        while True:
            # Capture a frame
            ret, frame = self.cap.read()

            # Check if the frame is captured successfully
            if not ret:
                print("Error: Unable to capture frame.")
                break

            # Display the frame
            cv2.imshow("Real-time Camera View", cv2.flip(frame, 1))  # Flip horizontally for mirror effect

            # Check for key press
            key = cv2.waitKey(1)
            if key == 13:  # Enter key
                # Flip the image horizontally before saving
                flipped_frame = cv2.flip(frame, 1)

                # Save the flipped frame as a jpg file
                cv2.imwrite("captured_image.jpg", flipped_frame)
                print("Image captured and saved as captured_image.jpg")
                break

        # Release the camera and close OpenCV windows
        self.cap.release()
        cv2.destroyAllWindows()
