import os
import tkinter as tk
from PIL import Image, ImageTk
from image_processor import ImageProcessor

class ImageDescriber:
    def __init__(self, api_key):
        self.image_processor = ImageProcessor(api_key)
        self.image_path = ""
        self.prompt = ""
        self.root = None
        self.image_label = None
        self.description_label = None
        self.callback = None

    def show_image_description(self):
        image_data_base64 = self.image_processor.encode_image_to_base64(self.image_path)
        answer = self.image_processor.image_question(prompt=self.prompt, data=image_data_base64)
        image = Image.open(self.image_path)
        # image = image.resize((600, 600))
        photo = ImageTk.PhotoImage(image)
        self.image_label.config(image=photo)
        self.image_label.image = photo
        self.description_label.config(text=answer)
        return answer

    def describe_image_with_tkinter(self, image_path, prompt, callback=None):
        self.image_path = image_path
        self.prompt = prompt
        self.callback = callback

        if not os.path.exists(image_path):
            print("The specified image path does not exist.")
            return

        self.root = tk.Tk()
        self.root.title("Image Describer")

        # Create labels for image and description
        self.image_label = tk.Label(self.root)
        self.image_label.pack()
        self.description_label = tk.Label(self.root, wraplength=400)
        self.description_label.pack()

        # Show the image and its description
        answer = self.show_image_description()

        # Destroy the root window when closed
        self.root.protocol("WM_DELETE_WINDOW", self.on_close)

        self.root.mainloop()

        # Return the answer
        return answer

    def on_close(self):
        self.root.destroy()
        if self.callback:
            self.callback()