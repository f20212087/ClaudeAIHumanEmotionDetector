class EmotionHandler:
    def get_message(self, emotion):
        if emotion == "Angry":
            return "Please tell me a funny joke"
        elif emotion == "Happy":
            return "Tell that I have a beautiful smile in a nice way"
        else:  # Emotion is "sad" or "neutral"
            return "Please give me an inspirational quote, dont explain it"